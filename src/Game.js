import React from 'react';
import './style.css'
export default class Game extends React.Component{
  constructor(props){
    super(props);
    this.state={
      guess:"",
      answerFetched:false,
      guessVariation:"",
      guessNumber:"",
      gameTerminated:false
    }
  }
  handleChange = (e) =>{
    e.persist();
    this.setState({guess:e.target.value})
  }
  handleSubmit = () => {
    if(this.state.guess >=1 && this.state.guess <=10){
        this.matchTheGuess();
         }else{
        alert("Please Enter a Number between 1 to 10");
      }
  }

  matchTheGuess=()=>{
    fetch("http://localhost:8080/guessgame/verify/"+this.state.guess)
    .then(response=>{let statusCode = response.status;
       let data = response.json();
      return Promise.all([statusCode, data]);})
    .then(
      (result) => {

        this.setState({
          answerFetched: true,
          guessVariation: result[1].correctness,
          guessNumber: result[1].number,
          gameTerminated: result[1].gameTerminated
        });

      },
      (error) => {
        this.setState({
          answerFetched: false,guessNumber:"",
          guessVariation: ""
        });
      }
    )

  }
  render(){
    let showGuessResult;
    let showGameStatus;
      if(this.state.answerFetched===true){
        showGuessResult = <div>your guess for number {this.state.guessNumber} is {this.state.guessVariation}</div>
      }else{
        showGuessResult=<div></div>
      }
      if(this.state.gameTerminated ){
        if( this.state.guessVariation==="CORRECT"){
          showGameStatus=<div>Right! you have won the Game. You can start next game by entering new guess.</div>;
        }else{
        showGameStatus=<div>Game Over. You can start next game by entering new guess.</div>;
       }
      }
    return(
      <div >
        <h4>I am thinking a number from 1 to 10. <br/>You must guess what it is in three tries.<br/></h4>
        <h3>Enter a guess
        <input onChange={this.handleChange}/><button onClick={this.handleSubmit}>Guess</button>
     <div> {showGuessResult}</div>
     <div> {showGameStatus}</div></h3>
      </div>
    )
  }
}