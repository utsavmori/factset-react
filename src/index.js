import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Game from './Game';
import './style.css';

class App extends Component {


  render() {
    return (
      <div className="App-header">
        <Game />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
